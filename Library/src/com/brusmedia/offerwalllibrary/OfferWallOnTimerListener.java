package com.brusmedia.offerwalllibrary;

public interface OfferWallOnTimerListener {
	void onOpening();
	void onClosing();
}