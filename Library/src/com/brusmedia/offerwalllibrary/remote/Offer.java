package com.brusmedia.offerwalllibrary.remote;

import org.json.JSONObject;

public class Offer {

	public int OfferId;
	public String Name;
	public String NameLabel;
	public double Price;
	public String PriceLabel;
	public String Description;
	public String TrackingURL;
	public String ThumbnailURL;
	
	public Offer() {}
	
	public static Offer fromJson(JSONObject object) {
		Offer o = new Offer();
		o.OfferId = object.optInt("offerId", 0);
		o.Name = object.optString("name","");
		o.NameLabel = object.optString("nameLabel","");
		o.Price = object.optDouble("price",0.0d);
		o.PriceLabel = object.optString("priceLabel","");
		o.Description = object.optString("description","");
		o.TrackingURL = object.optString("trackingLinkUrl","");
		o.ThumbnailURL = object.optString("thumbnailUrl","");
		return o;
	}
}
