package com.brusmedia.offerwalllibrary.remote;

import java.util.List;

public class ServiceResponseWrapper {

	public boolean IsSuccess = true;
	public List<Offer> Offers = null;
	public RemoteServiceException Exception = null;
	
	public static ServiceResponseWrapper getSuccess(List<Offer> offers) {
		ServiceResponseWrapper sr = new ServiceResponseWrapper();
		sr.IsSuccess = true;
		sr.Offers = offers;
		sr.Exception = null;		
		return sr;
	}
	
	public static ServiceResponseWrapper getFailure(RemoteServiceException ex) {
		ServiceResponseWrapper sr = new ServiceResponseWrapper();
		sr.IsSuccess = false;
		sr.Offers = null;
		sr.Exception =ex;		
		return sr;
	}
	
}
