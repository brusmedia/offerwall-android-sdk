package com.brusmedia.offerwalllibrary.remote;

public class RemoteServiceException extends Exception {
	
	private static final long serialVersionUID = 7919975602147687001L;
	private int statusCode = 0;

	public RemoteServiceException(String statusMessage, int statusCode) {
		super(statusMessage);
		this.statusCode = statusCode;
	}

	public RemoteServiceException(String statusMessage, int statusCode, Throwable throwable) {
		super(statusMessage, throwable);
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}
}
