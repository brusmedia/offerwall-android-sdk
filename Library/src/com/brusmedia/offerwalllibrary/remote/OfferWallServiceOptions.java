package com.brusmedia.offerwalllibrary.remote;

public class OfferWallServiceOptions {

	private String ServiceURL;
	private int TimeoutMilliseconds;

	public OfferWallServiceOptions(String serviceURL, int apiVersion, int timeoutMilliseconds) {
		super();
		ServiceURL = serviceURL + apiVersion;
		TimeoutMilliseconds = timeoutMilliseconds;
	}
	
	private static String PATH_COUNTRY_DEVICE = "%s/offers/byCountryDevice";
	public String getCountryDeviceURL() {
		return String.format(PATH_COUNTRY_DEVICE, ServiceURL);
	}
	
	public int getTimeoutMilliseconds() {
		return TimeoutMilliseconds;
	}
	
	public static OfferWallServiceOptions defaultOptionsVersion1 = new OfferWallServiceOptions("http://offerwallapi.brusmedia.com/api/v",1,60 * 1000);
	
}
