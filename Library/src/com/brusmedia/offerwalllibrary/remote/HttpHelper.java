package com.brusmedia.offerwalllibrary.remote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class HttpHelper {
	
	public static JSONArray getJsonArrayFromResource(Context context, int resourceId) throws JSONException, IOException {
		InputStream is = context.getResources().openRawResource(resourceId);
		return new JSONArray(getStringFromInputStream(is));
	}

	public static JSONObject getJsonObjectFromResource(Context context, int resourceId) throws JSONException, IOException {
		InputStream is = context.getResources().openRawResource(resourceId);
		return new JSONObject(getStringFromInputStream(is));
	}

	public static JSONArray getJsonArrayFromUrl(String url) throws MalformedURLException, JSONException, IOException {
		return getJsonArrayFromUrl(url, 0, 0);
	}

	public static JSONArray getJsonArrayFromUrl(String url, int connectTimeout, int readTimeout) throws MalformedURLException, JSONException, IOException {
		return new JSONArray(getStringFromUrl(url, connectTimeout, readTimeout));
	}

	public static JSONObject getJsonObjectFromUrl(String url) throws MalformedURLException, JSONException, IOException 
	{
		return getJsonObjectFromUrl(url, 0, 0);
	}

	public static JSONObject getJsonObjectFromUrl(String url, int connectTimeout, int readTimeout) throws MalformedURLException, JSONException, IOException {
		return new JSONObject(getStringFromUrl(url, connectTimeout, readTimeout));
	}
	
	public static JSONObject getJsonObjectFromUrlPost(String url, Map<String,String> parameters) throws MalformedURLException, JSONException, IOException {
		return new JSONObject(getStringFromUrlPost(url, parameters, 0, 0));
	}
	
	public static JSONObject getJsonObjectFromUrlPost(String url, Map<String,String> query, int connectTimeout, int readTimeout) throws MalformedURLException, JSONException, IOException, URISyntaxException {
		return new JSONObject(getStringFromUrlPost(url, query, connectTimeout, readTimeout));
	}

	public static String getStringFromInputStream(InputStream is) throws IOException {	
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = "";
		while((line = br.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

	public static String getStringFromUrl(String url, int connectTimeout, int readTimeout) throws MalformedURLException, JSONException, IOException {
		
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, connectTimeout);
		HttpConnectionParams.setSoTimeout(httpParameters, readTimeout);
		HttpClient client = new DefaultHttpClient(httpParameters);
		
        HttpResponse response = client.execute(new HttpGet(url));
        if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
        {
            throw new IOException(response.getStatusLine().getReasonPhrase());
        }
        
        return getStringFromInputStream(response.getEntity().getContent());
	}
	
	
	public static String getStringFromUrlPost(String url, Map<String,String> query, int connectTimeout, int readTimeout) throws MalformedURLException, IOException {
		
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, connectTimeout);
		HttpConnectionParams.setSoTimeout(httpParameters, readTimeout);
		HttpClient client = new DefaultHttpClient(httpParameters);
		
        HttpPost request = new HttpPost(url);
        
        if (query.size() > 0) {
        	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(query.size());
        	for (Map.Entry<String,String> entry : query.entrySet()) {
        		BasicNameValuePair x = new BasicNameValuePair(entry.getKey(), entry.getValue());
        		nameValuePairs.add(x);
        	}
        	request.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
        }
        
        HttpResponse response = client.execute(request);
        if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
        {
            throw new IOException(response.getStatusLine().getReasonPhrase());
        }

        return getStringFromInputStream(response.getEntity().getContent());
	}
	
    static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    
    static String urlEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) sb.append("&");
            sb.append(String.format("%s=%s",
                urlEncodeUTF8(entry.getKey().toString()),
                urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();       
    }
 }