package com.brusmedia.offerwalllibrary.remote;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OfferWallService {

	public static List<Offer> GetOffersByDeviceCountry(String url, int timeoutMilliseconds, String apiKey, String countryCode, String deviceType, String deviceId) throws RemoteServiceException {
		
		Map<String,String> params = new HashMap<String,String>();
		params.put("apiKey", apiKey);
		params.put("countryCode", countryCode.toUpperCase());
		params.put("device", deviceType);
		params.put("deviceId", deviceId);
		
		try {
			JSONObject response = HttpHelper.getJsonObjectFromUrlPost(url,params,timeoutMilliseconds, timeoutMilliseconds);
			JSONObject serviceData = getData(response);
			JSONArray serviceOffers = serviceData.getJSONArray("offers");
			List<Offer> offers = new ArrayList<Offer>(serviceOffers.length());
			for (int i = 0; i< serviceOffers.length(); i++) {
				offers.add(Offer.fromJson(serviceOffers.getJSONObject(i)));
			}
			return offers;
		} catch (JSONException je) {
			throw new RemoteServiceException("Could not parse offers from response", -1, je);
		} catch (UnknownHostException uhe) {
			throw new RemoteServiceException("Error getting results, no network connection", -1, uhe);
		} catch (RemoteServiceException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new RemoteServiceException("Error getting results", -1, ex);
		}
	}
	
	private static JSONObject getData(JSONObject response) throws RemoteServiceException {
		try {
			if (response != null) {
				if (!response.getBoolean("success")) {
					String statusMessage = response.optString("statusMessage","");
					int statusCode = response.optInt("statusCode", 0);				 
					throw new RemoteServiceException(statusMessage, statusCode);
				} else {
					return response.getJSONObject("data");
				}
			} else {
				throw new RemoteServiceException("Response Empty", -1);
			}
		} catch (JSONException je) {
			throw new RemoteServiceException("Could not parse response", -1, je);
		}
	}
	
	
}
