package com.brusmedia.offerwalllibrary;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

public class OfferWallTimer {

	private static OfferWallTimer instance = null;
	private static final String TAG = "OfferWallTimer";
	
	private Intent intent;
	private int intervalSeconds;
	private Context context;
	
	private int nextIntervalSeconds;
	private Handler handler;
	private Date startTime;
	private boolean isInitialised = false;
	
	private OfferWallOnTimerListener listener;
	
	protected OfferWallTimer() {}
	
	public static OfferWallTimer getInstance() {
		if (instance == null) {
			instance = new OfferWallTimer();
		}
		return instance;
	}
		
	public void start(final Context context, final Intent intent, int intervalSeconds, OfferWallOnTimerListener listener) {
		Log.i(TAG, "Start");
		this.context = context;
		this.intent = intent;		
		this.intervalSeconds = intervalSeconds;
		this.nextIntervalSeconds = intervalSeconds;
		this.listener = listener;
		isInitialised = true;		
		startWithInterval(intervalSeconds);
	}
	
	public void stop() {
		Log.i(TAG, "Cancel");
		clearHandler();
		intent = null;
		context = null;
		listener = null;
		isInitialised = false;
	}
	
	private void clearHandler() {
		if (handler != null) {
			handler.removeCallbacksAndMessages(null);
			handler = null;
		}
	}
	
	public void pause() {
		Log.i(TAG, "Pause");
		if (!isInitialised) throw new IllegalStateException("Timer not initalised with start(...)");
		clearHandler();
		Date now = Calendar.getInstance().getTime();
		nextIntervalSeconds = (int) ((now.getTime() - startTime.getTime()) / 1000);
		Log.i(TAG, "Remaining Seconds is " + nextIntervalSeconds);
	}
	
	public void resume() {
		Log.i(TAG, "Resume");
		if (!isInitialised) throw new IllegalStateException("Timer not initalised with start(...)");
		startWithInterval(nextIntervalSeconds);
		nextIntervalSeconds = intervalSeconds;
	}
	
	public void restart() {
		Log.i(TAG, "Restart");
		if (!isInitialised) throw new IllegalStateException("Timer not initalised with start(...)");
		startWithInterval(intervalSeconds);
	}
	
	public boolean isInitialised() {
		return isInitialised;
	}
	
	public void closeOfferWall() {
		if (listener != null)
			listener.onClosing();
	}
	
	private void startOfferWall() {
		if (listener != null)
			listener.onOpening();
		intent.putExtra(OfferWallActivity.PARAM_FROM_TIMER, true);
		context.startActivity(intent);
	}
	
	private void startWithInterval(int interval) {
		clearHandler();
		handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				startOfferWall();
			}
		}, interval * 1000);
		startTime = Calendar.getInstance().getTime();
	}
	
}
