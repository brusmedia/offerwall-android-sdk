package com.brusmedia.offerwalllibrary;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.brusmedia.offerwalllibrary.remote.OfferWallServiceOptions;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class OfferWallLauncher {

	public static final String BUNDLE_KEY = "data";
	
	public static final String PARAM_API_KEY = "apiKey";
	public static final String PARAM_COUNTRY_CODE = "countryCode";
	public static final String PARAM_DEVICE_TYPE = "deviceType";
	public static final String PARAM_DEVICE_ID = "deviceId";
	public static final String PARAM_SCREEN_FULL_SCREEN = "fullScreen";
	
	static final String PARAM_METHOD = "method";
	static final String PARAM_OPTION_SERVICE_URL = "serviceURL";
	static final String PARAM_OPTION_SERVICE_TIMEOUT = "serviceTimeout";
	static final String PARAM_METHOD_COUNTRY_DEVICE = "findByCountryDevice";
	
	public static Map<String,String> OfferWallDefaultParams(String apiKey) {
		Map<String,String> params = new HashMap<String,String>(1);
		params.put(PARAM_API_KEY, apiKey);
		return params;
	}
	
	public static Intent offerWallByCountryDevice(Context context, Map<String,String> params, OfferWallServiceOptions options) {
		
		if (context == null) throw new InvalidParameterException("Parameter context cannot be null");
		if (params == null) throw new InvalidParameterException("Parameter params cannot be null");
		if (!params.containsKey(PARAM_API_KEY)) throw new InvalidParameterException("Parameter params missing key " + PARAM_API_KEY);
		
		params.put(PARAM_METHOD,PARAM_METHOD_COUNTRY_DEVICE);
		params.put(PARAM_OPTION_SERVICE_URL, options.getCountryDeviceURL());
		params.put(PARAM_OPTION_SERVICE_TIMEOUT, String.valueOf(options.getTimeoutMilliseconds()));
		
		if (!params.containsKey(PARAM_COUNTRY_CODE)) {
			params.put(PARAM_COUNTRY_CODE, discoverCountryCode(context));
		}
		if (!params.containsKey(PARAM_DEVICE_TYPE)) {
			params.put(PARAM_DEVICE_TYPE, discoverDeviceType(context));
		}
		if (!params.containsKey(PARAM_DEVICE_ID)) {
			params.put(PARAM_DEVICE_ID, discoverDeviceId(context));
		}
		if (!params.containsKey(PARAM_SCREEN_FULL_SCREEN)) {
			params.put(PARAM_SCREEN_FULL_SCREEN, "true");
		}		
		Intent i = new Intent(context, OfferWallActivity.class);
		i.putExtra(BUNDLE_KEY, createBundle(params));
		return i;		
	}
	
	private static Bundle createBundle(Map<String,String> params) {
		Bundle b = new Bundle();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			b.putString(entry.getKey(), entry.getValue());
		}
		return b;
	}
	
	public static Map<String,String> createMap(Bundle b) {
		Map<String,String> params = new HashMap<String,String>(b.size());
		for (String key : b.keySet()) {
			params.put(key, b.getString(key));
		}
		return params;
	}
	
	private static String discoverCountryCode(Context context) {
		String countryCode;
		try {
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			countryCode = tm.getNetworkCountryIso();
			if (countryCode == null || countryCode.length() == 0) throw new Exception();
		} catch (Exception ex) {
			countryCode = Locale.getDefault().getCountry();
		}
		if (countryCode == null || countryCode.length() == 0) {
			return "US";
		} else {
			return countryCode;
		}
	}
	
	private static String discoverDeviceType(Context context) {
		return "Android";
	}
	
	private static String discoverDeviceId(Context context) {
		String permission = "android.permission.READ_PHONE_STATE";
	    int res = context.checkCallingOrSelfPermission(permission);
	    if (res == PackageManager.PERMISSION_GRANTED) {
	    	return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID); 
	    } else{
	    	return "";
	    }
	}
	
}
