package com.brusmedia.offerwalllibrary;

import java.lang.ref.SoftReference;
import java.util.concurrent.ConcurrentHashMap;

import android.graphics.Bitmap;

public class OfferWallImageCache {

	private static OfferWallImageCache cacheClass = null;
	private ConcurrentHashMap<String, SoftReference<Bitmap>> imageCache = new ConcurrentHashMap<String, SoftReference<Bitmap>>( 1 );
	private OfferWallImageCache() {}
	
	public static OfferWallImageCache getInstance() {
		if (cacheClass == null) {
			cacheClass = new OfferWallImageCache();
		}
		return cacheClass;
	}
	
	public Bitmap getImage(String key) {
		SoftReference<Bitmap> sr = imageCache.get(key);
		if (sr != null) return sr.get();
		return null;
	}
	
	public void putImage(String key, Bitmap img) {
		imageCache.put(key, new SoftReference<Bitmap>(img));
	}
}
