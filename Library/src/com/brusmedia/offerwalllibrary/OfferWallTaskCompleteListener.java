package com.brusmedia.offerwalllibrary;

import com.brusmedia.offerwalllibrary.remote.ServiceResponseWrapper;

public interface OfferWallTaskCompleteListener {
	public void onTaskComplete(ServiceResponseWrapper srw);
}
