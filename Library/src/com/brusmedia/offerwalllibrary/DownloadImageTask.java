package com.brusmedia.offerwalllibrary;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	
	ImageView bmImage;
	boolean setVisibility = true;
	String url = null;

	public DownloadImageTask(ImageView bmImage, boolean setVisibiltyWhenFinished) {
		this.bmImage = bmImage;
		this.setVisibility = setVisibiltyWhenFinished;
	}

	protected Bitmap doInBackground(String... urls) {
		url = urls[0];
		Bitmap mIcon11 = OfferWallImageCache.getInstance().getImage(url);
		if (mIcon11 == null) {
			try {
				InputStream in = new java.net.URL(url).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("DownloadImageTask", e.getMessage());
				e.printStackTrace();
			}
		} else {
			Log.i("DownloadImageTask", "image cache hit");
		}
		return mIcon11;		
	}

	protected void onPostExecute(Bitmap result) {
		if (result != null) {
			OfferWallImageCache.getInstance().putImage(url, result);
			bmImage.setImageBitmap(result);
			if (setVisibility) {
				bmImage.setVisibility(View.VISIBLE);
			}
		}
	}
}
