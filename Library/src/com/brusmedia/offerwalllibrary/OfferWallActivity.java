package com.brusmedia.offerwalllibrary;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import android.util.Log;

import com.brusmedia.offerwalllibrary.R;
import com.brusmedia.offerwalllibrary.R.layout;
import com.brusmedia.offerwalllibrary.remote.Offer;
import com.brusmedia.offerwalllibrary.remote.ServiceResponseWrapper;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class OfferWallActivity extends Activity {

	private static final String TAG = "OfferWallActivity";
	public static final String PARAM_FROM_TIMER = "from_timer";
	Map<String,String> params = null;
	OfferWallExecuteTask task = null;
	ListView offerWallList = null;
	ViewFlipper viewFlipper = null;
	TextView pageTitle = null;
	TextView loadingMsg = null;
	Typeface brusFont = null;
	Typeface brusFontSemiBold = null;
	Boolean fromTimer = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getBundleExtra(OfferWallLauncher.BUNDLE_KEY);
		params = OfferWallLauncher.createMap(b);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		if (Boolean.parseBoolean(params.get(OfferWallLauncher.PARAM_SCREEN_FULL_SCREEN))) {
			this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		} else {
			this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
		}
		setContentView(R.layout.activity_offer_wall);
		
		fromTimer = getIntent().getBooleanExtra(PARAM_FROM_TIMER,  false);		
		offerWallList = (ListView) findViewById(R.id.offer_wall_list);		
		viewFlipper = (ViewFlipper) findViewById(R.id.offer_wall_flipper);
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.fade));
				
		loadingMsg = (TextView) findViewById(R.id.txtLoadingGames);
		pageTitle = (TextView) findViewById(R.id.offer_wall_title);
		
		task = new OfferWallExecuteTask();
		task.setOfferWallExecuteTaskComplete(taskCompleteListener);
		task.execute(params);
		
		brusFont = getFontFromRes(R.raw.neris_light);
		brusFontSemiBold = getFontFromRes(R.raw.neris_semi_bold);
		if (brusFont != null) {
			loadingMsg.setTypeface(brusFont);
			pageTitle.setTypeface(brusFont);
		}
		
		View closeButton = findViewById(R.id.offer_wall_close_button);
		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				endActivity();
			}
		});
		
	}	
	
	private void endActivity() {
		setResult(RESULT_OK);
		if (fromTimer) {
			OfferWallTimer owt = OfferWallTimer.getInstance();
			owt.closeOfferWall();
			if (owt.isInitialised()) {
				owt.restart();
			}
		}
		finish();		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	endActivity();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (fromTimer) {
			OfferWallTimer owt = OfferWallTimer.getInstance();
			if (owt.isInitialised())
				owt.pause();
		}
	}
	
	private Typeface getFontFromRes(int resource)
	{ 
	    Typeface tf = null;
	    InputStream is = null;
	    try {
	        is = getResources().openRawResource(resource);
	    }
	    catch(NotFoundException e) {
	        Log.e(TAG, "Could not find font in resources!");
	    }
	    String outPath = getCacheDir() + "/tmp" + System.currentTimeMillis() + ".raw";
	    try
	    {
	        byte[] buffer = new byte[is.available()];
	        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outPath));
	        int l = 0;
	        while((l = is.read(buffer)) > 0)
	            bos.write(buffer, 0, l);
	        bos.close();
	        tf = Typeface.createFromFile(outPath);
	        new File(outPath).delete();
	    }
	    catch (IOException e)
	    {
	        Log.e(TAG, "Error reading in font!");
	        return null;
	    }
	    Log.d(TAG, "Successfully loaded font.");
	    return tf;      
	}
	
	private OfferWallTaskCompleteListener taskCompleteListener = new OfferWallTaskCompleteListener() {
		@Override
		public void onTaskComplete(ServiceResponseWrapper srw) {
			if (srw.IsSuccess) {
				ListRowAdapter adapter = new ListRowAdapter(srw.Offers, getLayoutInflater(), brusFont, brusFontSemiBold);
				offerWallList.setAdapter(adapter);
				viewFlipper.showNext();
			} else {
				loadingMsg.setText(srw.Exception.getMessage());
			}
		}
	};	

	private class ListRowAdapter extends BaseAdapter {
		
		List<Offer> offers;		
		LayoutInflater inflater;
		Typeface font;
		Typeface fontSemiBold;
		
		public ListRowAdapter(List<Offer> offers, LayoutInflater inflater, Typeface font, Typeface fontSemiBold) {
			this.offers = offers;
			this.inflater = inflater;
			this.font = font;
			this.fontSemiBold = fontSemiBold;
		}
		
		@Override
		public int getCount() {
			return offers.size();
		}

		@Override
		public Object getItem(int position) {
			return offers.get(position);
		}

		@Override
		public long getItemId(int position) {
			return offers.get(position).OfferId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = inflater.inflate(R.layout.offer_wall_row, parent, false);
			}
			
			final Offer offer = offers.get(position);
			
			TextView txtTitle = (TextView) view.findViewById(R.id.txtTitle);
			txtTitle.setText(offer.NameLabel);
			if (font != null)
				txtTitle.setTypeface(font);
			
			TextView txtPrice = (TextView) view.findViewById(R.id.txtPrice);
			String price = offer.PriceLabel.equals("FREE") ? "Free" : offer.PriceLabel;
			txtPrice.setText(price);
			if (fontSemiBold != null)
				txtPrice.setTypeface(fontSemiBold);
			
			ImageView thumbImage = (ImageView) view.findViewById(R.id.imgThumbnail);
			thumbImage.setVisibility(View.INVISIBLE);
			if (offer.ThumbnailURL != null && offer.ThumbnailURL.length() > 0) {
				DownloadImageTask thumbImageTask = new DownloadImageTask(thumbImage, true);
				thumbImageTask.execute(offer.ThumbnailURL);
			}

			if (offer.TrackingURL != null && offer.TrackingURL.length() > 0) {
			view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(offer.TrackingURL));
					startActivity(intent);
					endActivity();
				}
			});
			} else {
				view.findViewById(R.id.layoutPrice).setVisibility(View.GONE);
			}
			
			return view;
		}
	}
	
}

