package com.brusmedia.offerwalllibrary;

import java.util.List;
import java.util.Map;

import android.os.AsyncTask;
import com.brusmedia.offerwalllibrary.remote.Offer;
import com.brusmedia.offerwalllibrary.remote.OfferWallService;
import com.brusmedia.offerwalllibrary.remote.RemoteServiceException;
import com.brusmedia.offerwalllibrary.remote.ServiceResponseWrapper;

public class OfferWallExecuteTask extends AsyncTask<Map<String, String>, Void, ServiceResponseWrapper> {
	
	OfferWallTaskCompleteListener taskCompleteListener = null;
	public void setOfferWallExecuteTaskComplete(OfferWallTaskCompleteListener taskComplete) {
		taskCompleteListener = taskComplete;
	}

	@Override
	protected ServiceResponseWrapper doInBackground(Map<String, String>... params) {

		Map<String, String> firstParam = params[0];
		String method = firstParam.get(OfferWallLauncher.PARAM_METHOD);
		try {
			if (method.equalsIgnoreCase(OfferWallLauncher.PARAM_METHOD_COUNTRY_DEVICE)) {
				List<Offer> offers = OfferWallService.GetOffersByDeviceCountry(
					firstParam.get(OfferWallLauncher.PARAM_OPTION_SERVICE_URL),
					safeParse(firstParam.get(OfferWallLauncher.PARAM_API_KEY), 60*1000),
					firstParam.get(OfferWallLauncher.PARAM_API_KEY),
					firstParam.get(OfferWallLauncher.PARAM_COUNTRY_CODE),
					firstParam.get(OfferWallLauncher.PARAM_DEVICE_TYPE),
					firstParam.get(OfferWallLauncher.PARAM_DEVICE_ID));
				return ServiceResponseWrapper.getSuccess(offers);
			}
		} catch (RemoteServiceException e) {
			return ServiceResponseWrapper.getFailure(e);
		}
		return ServiceResponseWrapper.getFailure(new RemoteServiceException("Missing key " + OfferWallLauncher.PARAM_METHOD + " in parameters", -1));
	}
	
	@Override
	protected void onPostExecute(ServiceResponseWrapper result) {
		super.onPostExecute(result);
		if (taskCompleteListener != null) {
			taskCompleteListener.onTaskComplete(result);
		}
	}

	private int safeParse(String s, int defaultValue) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
}
