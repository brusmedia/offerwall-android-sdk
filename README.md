# Brus Media OfferWall for Android
Use this library to integrate the OfferWall into your Android Project.

## Setup using Eclipse
1. Add the library project to Eclipse using the File->Import->Existing Android Code Into Workspace
2. Add the library project to your existing project's dependencies.  Project Properties -> Android -> Add 
3. Add the OfferWallActivty to your AndroidManifest.xml, see an exmple of this in the sample project's manifest.

## Using the OfferWall
See the sample project for a working example of the OfferWall.  You will need to provide a valid api key for the sample project, see constant API_KEY in MainActivity.java

###Load OfferWall by Country & Device Type

	Map<String,String> params = OfferWallLauncher.OfferWallDefaultParams("YOUR_API_KEY");
	Intent launch = OfferWallLauncher.offerWallByCountryDevice(context, params, OfferWallServiceOptions.defaultOptionsVersion1);
	startActivity(launch);

## OfferWall Configuration
The following configuration options are available 

	PARAM_COUNTRY_CODE - defaults to the country code returned by the Telephony Service with fallback to the device's locale.
	PARAM_DEVICE_TYPE - defaults to 'Android' this probably shouldn't be changed.
	PARAM_DEVICE_ID - defaults to the Device Id if the permission android.permission.READ_PHONE_STATE exists otherwise empty string.
	PARAM_SCREEN_FULL_SCREEN - defaults to 'true', if false the status bar is shown with the OfferWall

Override the default params as such

	Map<String,String> params = OfferWallLauncher.OfferWallDefaultParams("YOUR_API_KEY")
	params.put(OfferWallLauncher.PARAM_SCREEN_FULL_SCREEN, "false");

###Locking Screen Orentation
If you wish to lock the orentation of the OfferWall to match your app, modify your Activity definition in your AndroidManifest.xml

	eg. android:screenOrientation="portrait"


## Using the OfferWall on a timer
The OfferWall can be configured to run on a timer such that after a interval has elapsed the OfferWall will be displayed.  This is to allow for example the OfferWall to be displayed after several minutes of gameplay.  Only one timer instance can operate at a time.  The timer is automiatically stopped/restarted while it is displaying the OfferWall

###Starting a timer

	Map<String,String> params = OfferWallLauncher.OfferWallDefaultParams("YOUR_API_KEY");
	Intent launch = OfferWallLauncher.offerWallByCountryDevice(context, params, OfferWallServiceOptions.defaultOptionsVersion1);
	OfferWallTimer owt = OfferWallTimer.getInstance();
	int seconds = 60; // display every minute
	owt.start(v.getContext(), launch, seconds, new OfferWallOnTimerListener() {
		
		@Override
		public void onOpening() {
			Log.i("SAMPLE", "Offerwall Opening");						
		}
		
		@Override
		public void onClosing() {
			Log.i("SAMPLE", "Offerwall Closing");						
		}
	});

###Pausing a timer
This function preserves the duration already elapsed.  Call resume() to start the timer again.

	OfferWallTimer owt = OfferWallTimer.getInstance();
	if (owt.isInitialised()) owt.pause();

###Resuming a paused timer
Used in conjunction with pause() to continue the timer from a previous elapsed duration.

	OfferWallTimer owt = OfferWallTimer.getInstance();
	if (owt.isInitialised()) owt.resume()

###Restarting a stopped timer
Used in conjunction with pause().  Restarts a timer using the orignial interval, ignoring the previously elapsed duration before pause() has been called.

	OfferWallTimer owt = OfferWallTimer.getInstance();
	if (owt.isInitialised()) owt.restart();

###Stopping a timer
IMPORTANT - It is the implementors responsibility to stop the timer when it is no longer needed.  This should be done when the timer is no longer required or the parent activity is ending.  Once a timer has been stopped it can only be restarted using start(...) as it is no longer considered initialised.

	OfferWallTimer owt = OfferWallTimer.getInstance();
	if (owt.isInitialised()) owt.stop();


The Sample project has an example of a timer implementation.