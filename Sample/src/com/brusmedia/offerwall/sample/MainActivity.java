package com.brusmedia.offerwall.sample;

import java.util.Map;

import com.brusmedia.offerwalllibrary.OfferWallLauncher;
import com.brusmedia.offerwalllibrary.OfferWallOnTimerListener;
import com.brusmedia.offerwalllibrary.OfferWallTimer;
import com.brusmedia.offerwalllibrary.remote.OfferWallServiceOptions;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	public static final String API_KEY = "YOUR_API_KEY";
	
	@Override
	protected void onPause() {
		super.onPause();
		OfferWallTimer owt = OfferWallTimer.getInstance();
		if (owt.isInitialised()) {
			//owt.cancel();
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button launch = (Button) findViewById(R.id.butLaunch);
		launch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Map<String,String> params = OfferWallLauncher.OfferWallDefaultParams(API_KEY);
				Intent launch = OfferWallLauncher.offerWallByCountryDevice(v.getContext(), params, OfferWallServiceOptions.defaultOptionsVersion1);
				startActivity(launch);
			}
		});
		
		Button launchNoTitle = (Button) findViewById(R.id.butLaunchNoFullscreen);
		launchNoTitle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Map<String,String> params = OfferWallLauncher.OfferWallDefaultParams(API_KEY);
				params.put(OfferWallLauncher.PARAM_SCREEN_FULL_SCREEN, "false");  // ["true"=default|"false"]
				Intent launch = OfferWallLauncher.offerWallByCountryDevice(v.getContext(), params, OfferWallServiceOptions.defaultOptionsVersion1);
				startActivity(launch);
			}
		});
				
		final Button launchTimer = (Button) findViewById(R.id.butLaunchTimer);
		launchTimer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Map<String,String> params = OfferWallLauncher.OfferWallDefaultParams(API_KEY);
				Intent launch = OfferWallLauncher.offerWallByCountryDevice(v.getContext(), params, OfferWallServiceOptions.defaultOptionsVersion1);
				OfferWallTimer owt = OfferWallTimer.getInstance();
				if (owt.isInitialised())
					owt.stop();
				owt.start(v.getContext(), launch, 7, new OfferWallOnTimerListener() {
					
					@Override
					public void onOpening() {
						Log.i("SAMPLE", "Offerwall Opening");						
					}
					
					@Override
					public void onClosing() {
						Log.i("SAMPLE", "Offerwall Closing");						
					}
				});
				Toast.makeText(v.getContext(), "Started timer", Toast.LENGTH_SHORT).show();
			}
		});
		
		final Button pause = (Button) findViewById(R.id.butPause);
		pause.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				OfferWallTimer owt = OfferWallTimer.getInstance();
				if (owt.isInitialised()) {
					Toast.makeText(v.getContext(), "Paused", Toast.LENGTH_SHORT).show();
					owt.pause();
				}
			}
		});
		
		final Button resume = (Button) findViewById(R.id.butResume);
		resume.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				OfferWallTimer owt = OfferWallTimer.getInstance();
				if (owt.isInitialised()) {
					Toast.makeText(v.getContext(), "Resumed", Toast.LENGTH_SHORT).show();
					owt.resume();
				}
			}
		});
		
		final Button restart = (Button) findViewById(R.id.butRestart);
		restart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				OfferWallTimer owt = OfferWallTimer.getInstance();
				if (owt.isInitialised()) {
					Toast.makeText(v.getContext(), "Restarted", Toast.LENGTH_SHORT).show();
					owt.restart();
				}
			}
		});

		final Button stop = (Button) findViewById(R.id.butStop);
		stop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				OfferWallTimer owt = OfferWallTimer.getInstance();
				if (owt.isInitialised()) {
					Toast.makeText(v.getContext(), "Stopped", Toast.LENGTH_SHORT).show();
					owt.stop();
				}
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
